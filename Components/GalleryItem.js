import React from 'react';
import {
    TouchableOpacity,
    Text,
    Image,
    StyleSheet
} from 'react-native';

import {listImageSize, maxNameSize} from '../settings';

export default function GalleryItem(props) {
    const name = props.data.name.length > maxNameSize ? 'Image' : props.data.name;
    const source = {uri: props.data.image};
    const author = props.data.author;

    return (
        <TouchableOpacity style={styles.container} onPress={props.onPress}>
            <Text>{name}</Text>
            <Image source={source} style={styles.image}/>
            <Text>Author: {author}</Text>
        </TouchableOpacity>
    )

}

const styles = StyleSheet.create({
    container: {
        margin: 1,
        borderWidth: 0.5,
        borderColor: 'grey'
    },
    image: {
        flex: 1,
        resizeMode: 'stretch',
        ...listImageSize
    }
});
