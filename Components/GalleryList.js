import React from 'react';
import {
    View,
    FlatList,
    StyleSheet,
    Dimensions
} from 'react-native';

import GalleryItem from './GalleryItem';
import {getPhotoList} from '../api';
import {listImageSize} from '../settings';

const columns = Math.floor(Dimensions.get('window').width / listImageSize.width);

export default class List extends React.Component {

    static navigationOptions = {
        title: 'Gallery',
    };

    constructor(){
        super();
        this.state = {
            data: [],
            page: 1,
        }
    }

    componentDidMount(){
        this.loadImages()
    }

    loadImages(){
        getPhotoList(this.state.page).then(
            json => {
                let newData = json.map(photoData=>{
                    return {
                        id: photoData.id,
                        description: photoData.description,
                        url: photoData.urls.small,
                        author: photoData.user.username
                    }
                });
                this.setState(prevState => {
                    return {
                        data: prevState.data.concat(newData)
                    }
                });
            }
        );
    }

    renderItem(el) {
        const {navigate} = this.props.navigation;
        return(
            <GalleryItem
                data={{
                    name: el.item.description || 'Unnamed Image',
                    image: el.item.url,
                    author: el.item.author
                }}
                onPress={() => navigate('Image', {imageId: el.item.id})}
            />
        )
    }

    onEndList() {
        this.setState((prevState) => {
            return {
                page: prevState.page + 1
            }
        }, this.loadImages);
    }

    render() {
        return (
            <View style={styles.listContainer}>
                <FlatList
                    data={this.state.data}
                    renderItem={item=>this.renderItem(item)}
                    numColumns={columns}
                    keyExtractor={(item, index) => String(index)}
                    onEndReached={() => this.onEndList()}
                />
            </View>
        )
    }

}

const styles = StyleSheet.create({
    listContainer: {
        flex: 1,
        flexDirection:'column'
    }
});
