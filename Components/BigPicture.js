import React from 'react';
import {View, Image, StyleSheet, Dimensions} from 'react-native';

import {getPhoto} from '../api';

export default class BigPicture extends React.Component{

    static navigationOptions = {
        title: 'Image',
    };

    constructor(props){
        super();
        this.state = {
            image: {}
        }
        this.imageId = props.navigation.getParam('imageId');
    }

    componentDidMount(){
        getPhoto(this.imageId).then(
            json => {
                this.setState({image: {uri: json.urls.full}})
            }
        );
    }

    render() {
        return(
            <View style={styles.container}>
                <Image source={this.state.image} style={styles.image}/>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    image: {
        flex: 1,
        width: Dimensions.get('screen').width,
        height: Dimensions.get('screen').height,
        resizeMode: 'contain'
    }
});
