const token = '842032351cabae8e52daef2f333ecc9c01e5ceda3e046ce3e6c0a35ee4c06123';

async function getWithToken(query, json=true){
    let resp = await fetch(`${query}client_id=${token}`)
    if(json) {
        return await resp.json();
    }
    return resp;
}

async function getPhoto(photoId){
    return await getWithToken(`https://api.unsplash.com/photos/${photoId}?`);
}

async function getPhotoList(page=1){
    return await getWithToken(`https://api.unsplash.com/photos/?page=${page}&`);
}

module.exports = {
    getPhoto,
    getPhotoList
}
