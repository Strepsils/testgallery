
import BigPicture from './Components/BigPicture';
import GalleryList from './Components/GalleryList';

import {createStackNavigator, createAppContainer} from 'react-navigation';

const MainNavigator = createStackNavigator(
  {
    Gallery: {screen: GalleryList},
    Image: {screen: BigPicture},
  },
  {
    initialRouteName: 'Gallery',
  }
);

export default createAppContainer(MainNavigator);
