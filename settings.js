const listImageSize = {
    width: 200,
    height: 200
}

const maxNameSize = 30;

module.exports = {
    listImageSize,
    maxNameSize
}
